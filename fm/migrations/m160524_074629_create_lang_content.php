<?php

use yii\db\Migration;

/**
 * Handles the creation for table `lang_content`.
 * Has foreign keys to the tables:
 *
 * - `lang`
 */
class m160524_074629_create_lang_content extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('lang_content', [
            'id' => $this->primaryKey(),
            'lang' => $this->string(),
            'content' => $this->text(),
            'lang_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `lang_id`
        $this->createIndex(
            'idx-lang_content-lang_id',
            'lang_content',
            'lang_id'
        );

        // add foreign key for table `lang`
        $this->addForeignKey(
            'fk-lang_content-lang_id',
            'lang_content',
            'lang_id',
            'lang',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `lang`
        $this->dropForeignKey(
            'fk-lang_content-lang_id',
            'lang_content'
        );

        // drops index for column `lang_id`
        $this->dropIndex(
            'idx-lang_content-lang_id',
            'lang_content'
        );

        $this->dropTable('lang_content');
    }
}
