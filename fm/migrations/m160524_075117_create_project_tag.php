<?php

use yii\db\Migration;

/**
 * Handles the creation for table `project_tag`.
 * Has foreign keys to the tables:
 *
 * - `project`
 * - `tag`
 */
class m160524_075117_create_project_tag extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project_tag', [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `project_id`
        $this->createIndex(
            'idx-project_tag-project_id',
            'project_tag',
            'project_id'
        );

        // add foreign key for table `project`
        $this->addForeignKey(
            'fk-project_tag-project_id',
            'project_tag',
            'project_id',
            'project',
            'id',
            'CASCADE'
        );

        // creates index for column `tag_id`
        $this->createIndex(
            'idx-project_tag-tag_id',
            'project_tag',
            'tag_id'
        );

        // add foreign key for table `tag`
        $this->addForeignKey(
            'fk-project_tag-tag_id',
            'project_tag',
            'tag_id',
            'tag',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `project`
        $this->dropForeignKey(
            'fk-project_tag-project_id',
            'project_tag'
        );

        // drops index for column `project_id`
        $this->dropIndex(
            'idx-project_tag-project_id',
            'project_tag'
        );

        // drops foreign key for table `tag`
        $this->dropForeignKey(
            'fk-project_tag-tag_id',
            'project_tag'
        );

        // drops index for column `tag_id`
        $this->dropIndex(
            'idx-project_tag-tag_id',
            'project_tag'
        );

        $this->dropTable('project_tag');
    }
}
