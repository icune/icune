<?php

use yii\db\Migration;

/**
 * Handles the creation for table `project`.
 * Has foreign keys to the tables:
 *
 * - `lang`
 * - `lang`
 */
class m160524_074640_create_project extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'date_from' => $this->timestamp(),
            'date_to' => $this->timestamp(),
            'description' => $this->integer()->notNull(),
            'contribution' => $this->integer()->notNull(),
            'name' => $this->string(),
            'link' => $this->string(),
            'code_link' => $this->string(),
            'image' => $this->string(),
        ]);

        // creates index for column `description`
        $this->createIndex(
            'idx-project-description',
            'project',
            'description'
        );

        // add foreign key for table `lang`
        $this->addForeignKey(
            'fk-project-description',
            'project',
            'description',
            'lang',
            'id',
            'CASCADE'
        );

        // creates index for column `contribution`
        $this->createIndex(
            'idx-project-contribution',
            'project',
            'contribution'
        );

        // add foreign key for table `lang`
        $this->addForeignKey(
            'fk-project-contribution',
            'project',
            'contribution',
            'lang',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `lang`
        $this->dropForeignKey(
            'fk-project-description',
            'project'
        );

        // drops index for column `description`
        $this->dropIndex(
            'idx-project-description',
            'project'
        );

        // drops foreign key for table `lang`
        $this->dropForeignKey(
            'fk-project-contribution',
            'project'
        );

        // drops index for column `contribution`
        $this->dropIndex(
            'idx-project-contribution',
            'project'
        );

        $this->dropTable('project');
    }
}
