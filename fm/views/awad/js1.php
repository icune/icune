Wiggy = {
	/* Blinks jquery elem @el with @_class */
	blink:function(el, _class){
		if(!_class)
			_class = 'wrong';
		var inx = 0;
		var ivalId = setInterval(intCb, 300);
		function intCb(){
			el.toggleClass(_class);
			if(++inx == 6)
				clearInterval(ivalId);
		}

	},
	/* popup from top for interacting via messaging with user */
	msg:function(text, ok){
		if($('.popup').size()){
			$('.popup').html($('.popup').html() + '&gt;br /&lt;' + text);
			return;
		}
		var pp = $('&gt;div&lt;').addClass('popup').text(text).appendTo('body');
		if(ok)
			pp.addClass('ok');
		pp.animate({top:0}, 500, function(){
			setTimeout(function(){
				pp.remove();
			}, 3000);
		})
	},
	/*
		checks - function that helps to check forms
		@r - dict k - v, where all k must be defined in emps
		each input(input, textarea, select) must have name, which defined in @emps
		each elem in @emps is string if it name of input, array if array of names ::
			if array then elems are checked for one of elems is filles
		@regx is dict, where key - name, elem from keys of @r or keys of @emps
		@regex values are dicts, which keys must be defined:
			pre : function String -&lt; String, which makes preprocess for check after it string for regexp
			reg : regexp
			msg : message, which will be displayed if error was occured

		Thinks for future - multilanguage support must be realized via globals js
	*/
	check:function(e, r, emps, regx, elemGet){
		if(e instanceof Array){
			var was = false;
			var err_k = [];
			for(var k in e){
				if(!r[e[k]].trim())
					continue;
				
				if(r[e[k]].trim().length && regx[e[k]].pre(r[e[k]]).match(regx[e[k]].reg)){
					was = true;
				}else{
					err_k.push(e[k]);
				}
			}
			if(!was && !err_k.length){
					for(var k in e)
						Wiggy.blink(elemGet(e[k]));
					
				return false;
			}
			if(err_k.length){
				for(var k in err_k){	
					Wiggy.blink(elemGet(err_k[k]));
				};
				return false;
			}
			return true;
		}else{
			if(regx[e].pre(r[e]).match(regx[e].reg))
				return true;
			else{
				Wiggy.blink(elemGet(e));
				
				return false;
			};
		};
	},
	contacts:function(){
		
	},
	modal:function(title, text){
		$('.modal.wiggy .modal-title .text').text(title);
		$('.modal.wiggy .modal-data').html(text);
		$('.modal.wiggy').modal();
	}
}