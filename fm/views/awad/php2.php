namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image, App\ImageAttr;
class TestGenerator extends Model
{
	use App\Traits\QuestionGenerator;
    public function distribute($size, $values, $how){
        if($how == 'equal'){
            shuffle($values);
            $arr = [];
            $portionSize = (int)($size/count($values));
            if(!$portionSize) $portionSize = 1;
            $valuesIndex = -1;
            for($i = 0; $i < $size; $i++){
                if($i % $portionSize == 0 && $valuesIndex+1 < count($values))
                    $valuesIndex++;
                $arr []= $values[$valuesIndex];
            };
            shuffle($arr);
            return $arr;
        }else{
            $arr = [];
            for($i = 0; $i < $size; $i++)
                $arr []= $values[rand(0, count($values) - 1)];
            return $arr;
        }
    }

    public function genTest($params){
        $distEmos = $this->distribute(
            (int)$params['count'], 
            array_keys(\Shared::$emosTitle), 
            $params['distribution_emos']
        );
        $distKinds = $this->distribute(
            (int)$params['count'], 
            $params['kinds'], 
            $params['distribution_kinds']
        );
        $distSizes = $this->distribute(
            (int)$params['count'], 
            range((int)$params['size_from'], (int)$params['size_to']), 
            $params['distribution_size']
        );
        $questions = [];
        for($i = 0; $i < $params['count']; $i++)
            $questions []= $this->genQuestion([
                'required_emotion' => $distEmos[$i],
                'question_type' => $distKinds[$i],
                'size' => $distSizes[$i],
                'except_image_ids' => []
            ]);
        return $questions;
    }
}