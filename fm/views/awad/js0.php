function docsys(Container, Nodes, Callbacks){
	if(!Callbacks)
		Callbacks = [];
	var cbs = {
		add:function(){},
		render:function(){},
		struct:function(){}
	}

	initCbs();

	var rootBlock = null;
	setJson(Nodes);



	this.rollup = function(){
		rootBlock.find('.offseter').hide();
		rootBlock.find('.offseter').eq(0).show();
	};
	this.expand = function(){
		rootBlock.find('.offseter').show();
	}
	this.json = function(NJ){
		if(!NJ)
			return getJson();
		return setJson(NJ);
	}
	this.on = function(Name, Func){
		if(typeof Func !== 'function')
			return;
		cbs[Name] = Func;
	};
	function initCbs(){
		if(typeof Callbacks['add'] == 'function')
			cbs.add = Callbacks['add'];
		if(typeof Callbacks['render'] == 'function')
			cbs.render = Callbacks['render'];
		if(typeof Callbacks['struct'] == 'function')
			cbs.struct = Callbacks['struct'];
	}
	function thread(Func){
		setTimeout(Func, 0);
	}
	function getJson(){
		var offs = rootBlock.find('.offseter').eq(1);
		return rec(offs);
		function rec(Offs){
			var els = [];
			var chi = Offs.children();
			for(var k = 0; k &lt; chi.length; k++){
				if(chi.eq(k).hasClass('elem-space')){
					var tel = chi.eq(k).find('.tree-elem').eq(0);
					var nel = {id:tel.attr('id'), title:tel.find('.main-space').text().trim()};
					var offs = chi.eq(k).find('.offseter');
					if(offs.length)
						nel.child = rec(offs.eq(0));
					els.push(nel);
				};
			}
			return els;
		};
	};
	function setJson(J){
		Nodes = J;
		$(rootBlock).remove();
		rootBlock = constructBlocks();
		$(Container).append(rootBlock);
		attachEvents();
	}
	function calcOpenOpacity(Block){
		if(!Block)
			Block = rootBlock;
		$(Block).find('.tree-elem').each(
			function(){
				if($(this).next().hasClass('offseter'))
					$(this).find('.open-action').css('opacity', '1');
				else
					$(this).find('.open-action').css('opacity', '0.2');
			}
		);
	};

	function elemSpaceAndElem(Id, Title, Except){
		if(!Except)
			Except = [];
		if(Except.indexOf('all') != -1)
			Except = ['move', 'add', 'delete', 'space'];
		var controls = ""+
		"&lt;td class='tree-elem-button "+((Except.indexOf('move') !== -1)?'':'move-action')+"'&gt;&lt;/td&gt;"+
		"&lt;td class='tree-elem-button "+((Except.indexOf('add') !== -1)?'':'add-action')+"'&gt;&lt;/td&gt;"+
		"&lt;td class='tree-elem-button "+((Except.indexOf('delete') !== -1)?'':'delete-action')+"'&gt;&lt;/td&gt;";

		var elemSpace = $('&lt;div class="elem-space"&gt;&lt;/div&gt;');
		var intable = 
		"&lt;table&gt;\
			&lt;tr&gt;\
				&lt;td class='tree-elem-button open-action'&gt;&lt;/td&gt;\
				&lt;td class='main-space"+((Except.indexOf('space') !== -1)?"-index":"" )+"'&gt;\
				"+Title+"\
				&lt;/td&gt;\
				&lt;td&gt;\
					&lt;table class='button-table'&gt;\
						&lt;tr&gt;\
							\
							"+controls+"\
						&lt;/tr&gt;\
					&lt;/table&gt;\
				&lt;/td&gt;\
			&lt;/tr&gt;\
		&lt;/table&gt;";
		$("&lt;div class='tree-elem' id='"+Id+"' &gt;"+intable+"&lt;/div&gt;").appendTo(elemSpace);
		return elemSpace;
	}
	function levelBlock(){
		return $('&lt;div class="offseter"&gt;&lt;/div&gt;');
	}
	function constructBlocks(){
		var root = $('&lt;div class="menu-container"&gt;&lt;/div&gt;');
		Nodes = [{id:0, title:'Оглавление', child:Nodes}];
		recAdd(Nodes, 0, root);
		calcOpenOpacity(root);
		return root;
		function recAdd(List, Level, Block){
			if(!List)
				return;
			var lBlock = levelBlock(Level);
			for(var k in List){
				
				var elemSpace = elemSpaceAndElem(List[k]['id'], List[k]['title'], !Level?['delete', 'move','space']:[]);
				
				recAdd(List[k]['child'], Level+1, elemSpace);

				elemSpace.appendTo(lBlock);
				
			}
			lBlock.appendTo(Block)
		}
	}
	function addChild(Parent, Child){
		var trem = $(Parent).closest('.tree-elem');
				
		if(trem.next().hasClass('offseter')){
			trem.next().append(Child);

		}else{
			var lBlock = levelBlock().append(Child);
			trem.after(lBlock);
		};
		deatEvents();
		calcOpenOpacity();
	}
	function confirmStyle(X, Y, OnConfirm){
		var conf = $('&lt;div class="tree-elem" style="padding:5px"&gt;&lt;/div&gt;');
		$('body').append(conf);
		conf.append(
			$('&lt;div class="confirm-no" style="margin-right:10px;"&gt;&lt;/div&gt;').click(function(){
				conf.remove();
				
			})
		).
		append(
			$('&lt;div class="confirm-yes"&gt;&lt;/div&gt;').click(function(){
				
				conf.hide();
				setTimeout(function(){
					conf.remove();
				},1000);
				OnConfirm();
			})
		);
		conf.css({
			position:'absolute',
			left:X+'px',
			top:Y+'px',
			zIndex:100500
		});
		conf.mouseleave(function(){
			conf.remove();
		})
		
	}
	function attachEvents(){


		$('.tree-elem .main-space').on('click',function(){
			$('.tree-elem').removeClass('tree-elem-white');
			$(this).closest('.tree-elem').addClass('tree-elem-white');
			var id = $(this).closest('.tree-elem').attr('id');
			cbs.render(id);	
		});


		$('.tree-elem .delete-action').on('click',function(e){
			var trem = $(this).closest('.elem-space');
			var offs = $(this).closest('.offseter');
			if(offs.children().length == 1)
				trem = offs;
			confirmStyle(e.pageX-10, e.pageY-10, function(){
				trem.remove();
				calcOpenOpacity();
				cbs.struct();
			});
			
			
		});


		$('.tree-elem .add-action').on('click',function(){
			var nel = elemSpaceAndElem('New', 'New');
			addChild(this, nel);
			cbs.add(function(params){
				nel.find('.tree-elem').attr('id', params['id']);
				nel.find('.main-space').text(params['title']);
				cbs.struct();
			});

		});


		$('.tree-elem .open-action').on('click',function(e){
			var trem = $(this).closest('.tree-elem');
			if(trem.next().hasClass('offseter'))
				trem.next().toggle();

		});


		$('.tree-elem .move-action').on('click',function(){
			detachEvents();
			var esp = $(this).closest('.elem-space');
			var offs = $(this).closest('.offseter');
			var id = $(this).closest('.tree-elem').attr('id');
			function makeAbs(){
				esp.css({position:'absolute', width:esp.width()+'px', 'z-index':100500});
			};
			function makeStatic(){
				esp.css({position:'static', width:'', 'z-index':0});
			};
			makeAbs();

			$(window).on('mousemove', function(e){
				esp.css({'left':e.pageX+5+'px', 'top':e.pageY+5+'px', });
			});

			$('.tree-elem .add-action').on('click', function(){
				addChild(this, esp);
				makeStatic();
				deatEvents();
				if(offs.children().length == 0)
					offs.remove();
				calcOpenOpacity();
				cbs.struct();
				
			});

			$('.tree-elem .main-space').on('mouseenter', function(){
				var cesp = $(this).closest('.elem-space');
				cesp.addClass('between-hover-color');
			});
			$('.tree-elem .main-space').on('mouseleave', function(){
				var cesp = $(this).closest('.elem-space');
				cesp.removeClass('between-hover-color');
			});
			setTimeout(
				function(){
					$('.tree-elem .main-space').on('click', function(){
						var cesp = $(this).closest('.elem-space');
						cesp.removeClass('between-hover-color');
						cesp.after(esp);
						makeStatic();
						deatEvents();
						if(offs.children().length == 0)
							offs.remove();
						calcOpenOpacity();
						cbs.struct();
					});
				}
			,100)
			

		});
	}
	function detachEvents(){
		$('.tree-elem .add-action').off('click');
		$('.tree-elem .open-action').off('click');
		$('.tree-elem .move-action').off('click');
		$('.tree-elem .delete-action').off('click');
		$('.tree-elem .main-space').off('click');
		$('.tree-elem .main-space').off('mouseenter');
		$('.tree-elem .main-space').off('mouseleave');
		$('.tree-elem').off('click');
		$('.tree-elem').off('mouseenter');
		$('.tree-elem').off('mouseleave');
	}
	function deatEvents(){
		detachEvents();
		attachEvents();
	}

}