@extends('layouts.app')

@section('script')

@endsection

@section('content')
&lt;style&gt;
	.padded{
		padding-left: 30px;
	}
	.row{
		margin-bottom:20px;
	}
&lt;/style&gt;

&lt;div class="container"&gt;
    &lt;h1&gt;Создание теста&lt;/h1&gt;
    &lt;form id="test-form" method="POST" action='{{URL::action("TestManagerController@postPreview")}}'
    accept-charset="UTF-8"&gt;
        &lt;div class="row"&gt;
            {!! csrf_field() !!}
        	&lt;div class="col-md-4 padded"&gt;
        		Имя
        	&lt;/div&gt;
        	&lt;div class="col-md-8"&gt;
        		&lt;input type="text" class="form-control" name="name" value="Тест"/&gt;
        	&lt;/div&gt;
        &lt;/div&gt;
        &lt;div class="row"&gt;
            &lt;div class="col-md-4 padded"&gt;
                Есть ли тренировка перед тестом
            &lt;/div&gt;
            &lt;div class="col-md-8"&gt;
                &lt;div class="checkbox"&gt;
                  &lt;label&gt;&lt;input type="checkbox" name="train" value="pic_ans_" checked="checked"&gt;Флажок установлен - тренировка есть&lt;/label&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
        &lt;div class="row"&gt;
        	&lt;div class="col-md-4 padded"&gt;
        		Количество вопросов
        	&lt;/div&gt;
        	&lt;div class="col-md-8"&gt;
        		&lt;input type="number" min="1" max="100" class="form-control" name="count"  value="20"/&gt;
        	&lt;/div&gt;
        &lt;/div&gt;
        &lt;div class="row"&gt;
        	&lt;div class="col-md-4 padded"&gt;
        		Время, мин.
        	&lt;/div&gt;
        	&lt;div class="col-md-8"&gt;
        		&lt;input type="number" min="1" max="100" class="form-control" name="time" value="5"/&gt;
        	&lt;/div&gt;
        &lt;/div&gt;
        &lt;div class="row"&gt;
            &lt;div class="col-md-4 padded"&gt;
                Распределение эмоций
            &lt;/div&gt;
            &lt;div class="col-md-8"&gt;
                &lt;select name="distribution_emos" class="form-control"&gt;
                    &lt;option value="equal"&gt;Поровну&lt;/option&gt;
                    &lt;option value="random"&gt;Случайно&lt;/option&gt;
                &lt;/select&gt;
            &lt;/div&gt;
        &lt;/div&gt;
        &lt;div class="row"&gt;
            &lt;div class="col-md-4 padded"&gt;
                Типы вопросов
            &lt;/div&gt;
            &lt;div class="col-md-8"&gt;
                &lt;div class="checkbox"&gt;
                  &lt;label&gt;&lt;input type="checkbox" name="kinds[]" value="pic1ans1" checked="checked"&gt;Одна картинка - один правильный ответ&lt;/label&gt;
                &lt;/div&gt;
                &lt;div class="checkbox"&gt;
                  &lt;label&gt;&lt;input type="checkbox" name="kinds[]" value="pic_ans1" checked="checked"&gt;Много картинок - один правильный ответ&lt;/label&gt;
                &lt;/div&gt;
                &lt;!-- &lt;div class="checkbox"&gt;
                  &lt;label&gt;&lt;input type="checkbox" name="kinds[]" value="pic_ans_" checked="checked"&gt;Сопоставление нескольких картинок нескольким вариантам&lt;/label&gt;
                &lt;/div&gt; --&gt;
            &lt;/div&gt;
        &lt;/div&gt;
        &lt;div class="row"&gt;
            &lt;div class="col-md-4 padded"&gt;
                Распределение типов вопросов
            &lt;/div&gt;
            &lt;div class="col-md-8"&gt;
                &lt;select name="distribution_kinds" class="form-control"&gt;
                    &lt;option value="equal"&gt;Поровну&lt;/option&gt;
                    &lt;option value="random"&gt;Случайно&lt;/option&gt;
                &lt;/select&gt;
            &lt;/div&gt;
        &lt;/div&gt;
        &lt;div class="row"&gt;
            &lt;div class="col-md-4 padded"&gt;
                Размер выборки
                @include('widgets.help', ['text' =&gt; '
                    Количество правильных ответов.
                    Например, для варианта "Много картинок - один правильный ответ"
                    количество картинок будет равняться размеру выборки
                '])
            &lt;/div&gt;
            &lt;div class="col-md-8 form-inline"&gt;
                &lt;div class="form-group"&gt;
                    &lt;label style="font-weight:300" for="size_form"&gt;
                        От (2..7)
                    &lt;/label&gt;
                    &lt;input type="number" id="size_from" class="form-control" name="size_from" min="2" max="7" value="4"&gt;
                &lt;/div&gt;
                &lt;div class="form-group"&gt;
                    &lt;label style="font-weight:300" for="size_to"&gt;
                        До (2..7)
                    &lt;/label&gt;
                    &lt;input type="number" id="size_to" class="form-control" name="size_to" min="2" max="7" value="7"&gt;
                &lt;/div&gt;
            &lt;/div&gt;
        &lt;/div&gt;
        &lt;div class="row"&gt;
            &lt;div class="col-md-4 padded"&gt;
                Распределение размера выборки
            &lt;/div&gt;
            &lt;div class="col-md-8"&gt;
                &lt;select name="distribution_size" class="form-control"&gt;
                    &lt;option value="equal"&gt;Поровну&lt;/option&gt;
                    &lt;option value="random"&gt;Случайно&lt;/option&gt;
                &lt;/select&gt;
            &lt;/div&gt;
        &lt;/div&gt;
    &lt;/form&gt;
    &lt;form method="POST" action="{{URL::action("TestManagerController@postCreate")}}" id="create-form"&gt;
         {!! csrf_field() !!}
    &lt;/form&gt;
    &lt;div class="alert alert-danger fade in" id="error" style='display: none'&gt;
        &lt;a href="#" class="close" data-dismiss="alert" aria-label="close"&gt;&times;&lt;/a&gt;
        &lt;strong&gt;Вы заполнили форму с ошибками.&lt;/strong&gt;
        &lt;span class="text"&gt;&lt;/span&gt;
    &lt;/div&gt;
    &lt;center&gt;
        &lt;div class="btn btn-success" id="gen-test"&gt;
            Сгенерировать
        &lt;/div&gt;    
    &lt;/center&gt;
    @if(isset($preview) && count($preview))
        &lt;br&gt;
        &lt;center&gt;
            &lt;div class="btn btn-success" id="create-test"&gt;
                Создать
            &lt;/div&gt;    
        &lt;/center&gt;
        &lt;div class="question"&gt;
            @widget('questionwidget', ['training' =&gt; true, 'question' =&gt; $trainingQuestion])
        &lt;/div&gt;
        @foreach($preview as $k =&gt; $p)
            &lt;div class="question"&gt;
                @widget('questionwidget', ['question' =&gt; $p, 'no' =&gt; $k+1, 'of' =&gt; count($preview)])
            &lt;/div&gt;
        @endforeach
    @endif
&lt;/div&gt;
@endsection
