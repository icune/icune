class ControllerProductSearchAutoac extends Controller {
	public function index(){
		$attrs = [
			'tire' => [
			    'width' => 23,
			    'height' => 21,
			    'diameter' => 22,
			    'season' => 10,
			],
			'disc' => [
			    'width' => 27,
			    'diameter' => 26,
			    'pcd' => 16,
			    'dia' => 24,
			    'et' => 25,
			],
			'ttire' => [
				'width' => 36,
				'height' => 37,
				'diameter' => 35,
				'axle' => 40,
				'frameconstr' => 38,
			]
		];

		if(!isset($_POST['tab'])){
			echo '{}';
			exit;
		}
		if(!isset($attrs[$_POST['tab']])){
			echo '{}';
			exit;
		};

		$branch = $attrs[$_POST['tab']];
		$data = [];
		$rKey = null;
		foreach ($branch as $k => $v) {
			if(isset($_POST[$k]) && trim($_POST[$k])){
				$rKey = $k;
				$data[$v] = $_POST[$k];
			}
		}
		$manufacturer = isset($_POST['manufacturer']) ? $_POST['manufacturer'] : null;
		$sql = $this->fullSql($branch, $data, $manufacturer);
		// echo $sql;
		$rows = $this->db->query($sql)->rows;
		$r = [
			'count' => $this->db->query($this->countSql($data, $manufacturer, $_POST['tab']))->rows[0]['count'],
			'attrs' => $this->cvt($rows, $branch)
		];
		if(count($data) == 1){
			$r['attrs'][$rKey] = $this->forOne(array_keys($data)[0]);
		};
		echo json_encode($r);
	}

	private function cvt($arr, $branch){
		$r = [];
		foreach($branch as $k=>$v)
			$r [$k]= [];

		if(!count($arr))
			return $r;

		foreach($arr as $a)
			foreach($a as $k => $v)
				$r[$k] []= $v;
		foreach($r as $k => $v){
			$v = $this->process($v);
			$r[$k] = $v;
		}
		return $r;
	}

	private function fullSql($branch, $data, $manufacturer = null){
		$fsql = $this->filterSql($data, $manufacturer);
		$fields = ['m.name manufacturer'];
		foreach($branch as $name=>$id)
			$fields []= "pa$id.text $name";
		$sql = 'SELECT ' . implode(',', $fields) . ' FROM oc_product p ';
		$sql .= ' JOIN (' . $fsql . ') ft ON p.product_id = ft.product_id ';
		
		$selData = array_fill_keys($branch, 0);
		list($joins, $wheres) = $this->byAttributes($selData);
		list($joins_m, $wheres_m) = $this->byManufacturer($manufacturer);

		$joins = array_merge($joins, $joins_m);
		$wheres = array_merge($wheres, $wheres_m);
		$wheres = array_merge($wheres, array(' p.quantity > 0 '));

		$sql .= implode(' ', $joins);
		$sql .= (count($wheres) ? ' WHERE ' : '') . implode(' AND ', $wheres);
		return $sql;
	}
	private function filterSql($data, $manufacturer = null){
		list($joins, $wheres) = $this->byAttributes($data);
		list($joins_m, $wheres_m) = $this->byManufacturer($manufacturer);
		$joins = array_merge($joins, $joins_m);
		$wheres = array_merge($wheres, $wheres_m);
		$wheres = array_merge($wheres, array(' p.quantity > 0 '));
		$sql = "SELECT p.product_id product_id from oc_product p ";
		$sql .= implode(' ', $joins);
		$sql .= (count($wheres) ? ' WHERE ' : '') . implode(' AND ', $wheres);
		return $sql;
	}
	private function countSql($data, $manufacturer = null, $type='tire'){
		list($joins, $wheres) = $this->byAttributes($data);
		list($joins_m, $wheres_m) = $this->byManufacturer($manufacturer);
		$joins = array_merge($joins, $joins_m);
		$wheres = array_merge($wheres, $wheres_m);
		$wheres = array_merge($wheres, array(' p.quantity > 0 '));
		if(!count($data)){
			$cids = ['ttire' => 100, 'tire' => 90, 'disc' => 80];
			$joins []= ' JOIN oc_product_to_category ptc ON ptc.product_id=p.product_id ';
			$joins []= ' JOIN oc_category c ON c.category_id=ptc.category_id ';
			$wheres [] = '(c.parent_id=' . $cids[$type] . ')';
		};


		$sql = "SELECT count(*) count from oc_product p ";
		$sql .= implode(' ', $joins);
		$sql .= (count($wheres) ? ' WHERE ' : '') . implode(' AND ', $wheres);

		return $sql;
		
	}
	private function byAttributes($data){
		$db = $this->db;
		$joins = [];
		$wheres = [];
		foreach($data as $k=>$v){
			$joins  []= " JOIN oc_product_attribute pa$k ON pa$k.product_id = p.product_id ";
			$where = "pa$k.attribute_id=$k";
			if($v){
				$v = $db->escape($v);
				$where .= " AND pa$k.text='$v'";
			};
			$wheres []= '(' . $where . ')';
		}
		return [$joins, $wheres];
	}
	private function byManufacturer($manufacturer = null){
		$db = $this->db;
		$joins = [];
		$wheres = [];
		$joins []= " JOIN oc_manufacturer m ON m.manufacturer_id=p.manufacturer_id ";
		if($manufacturer){
			$manufacturer = $db->escape($manufacturer);
			$wheres []= "(m.name='$manufacturer')";
		}
		return [$joins, $wheres];
	}
	private function forOne($attrId){
		$sql = 'SELECT text FROM oc_product_attribute WHERE attribute_id = '.$attrId;
		$rows = $this->db->query($sql)->rows;
		$res = [];
		foreach($rows as $r)
			$res []= $r['text'];
		return $this->process($res);
	}
	private function ifCanToNumeric($arr){
		$iKeyArr = [];
		foreach($arr as $k => $a){
			$prep = trim(str_replace(',', '.', $a.''));
			if(preg_match('!^[-]?\\d+(\\.\\d+)?$!', $prep))
				$iKeyArr [$k]= (float)$prep;
		}
		if(count($iKeyArr) >= count($arr)/2){
			
			asort($iKeyArr);
			$res = [];
			foreach($iKeyArr as $k => $v)
				$res []= $arr[$k];
			return $res;
		}else{
			sort($arr);
			return $arr;
		};
	}
	private function process($arr){
		$arr = array_unique($arr);
		$arr = $this->ifCanToNumeric($arr);
		return $arr;
	}
}