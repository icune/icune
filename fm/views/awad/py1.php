import os
import shutil
ret = {}
inx = 0
if os.path.isdir('LL_'):
	shutil.rmtree('LL_')
os.mkdir('LL_/')
for r,d,f in os.walk('LL'):
	if r == 'LL':
		continue
	kKey = r.replace('LL\\', '')
	ret[kKey] = []
	_cd = 'LL_/' + str(inx) + '/'
	os.mkdir(_cd)
	_fInx = 0
	for _f in f:
		print _cd + str(_fInx)
		os.system('convert "%s" -resize 1000 "%s"' % (r + '/' + _f, _cd + str(_fInx) + '.png'))
		os.system('convert "%s" -resize 300 "%s"' % (r + '/' + _f, _cd + str(_fInx) + '_m.png'))
		ret[kKey] += [_cd + str(_fInx) + '.png']
		_fInx+= 1
	inx+=1
# print ret.keys()
import json
with open('struct.json', 'w') as f:
	f.write(json.dumps(ret, ensure_ascii=False))