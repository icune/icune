<?php
use app\assets\HighlightAsset;
use app\assets\AwadAsset;

HighlightAsset::register($this);
AwadAsset::register($this);
$title = 'Задание для AnyWayAnyDay';
$this->title = $title;
?>
<style>
	.quote{
		margin: 5px;
		padding:10px;
		border:1px solid silver;
		margin-bottom: 10px;
	}
	.open{
		cursor: pointer;
		text-decoration: underline;
	}
	.open:hover{
		color:white !important;
	}
	.open:hover span{
		color:white !important;
	}
	.desc{
		font-weight: bold;
	    background-color: silver;
	    padding: 5px;
	    border-radius: 3px;
	    cursor: pointer;
	}
	.invisible-code{
		display: none;
	}
</style>

<script type="text/javascript">
	function replaceMacro(){
		var code = $(this).closest('code');
		var mtc = code.html().match(/@([a-z]+)_([a-z_]+)(\:[a-z]+)?/g);
		var repStr = "\n\
		{table}_{index} INT,<br>\
		INDEX {table}_{index} ({table}_{index}),<br>\
		FOREIGN KEY ({table}_{index}) REFERENCES {ctable}({index}) ON DELETE CASCADE";
		for(var k in mtc){
			var thh = (mtc[k] + '').replace('@', '');
			var ctable = thh.split('_')[0];
			var table = ctable + '';


			if(thh.indexOf(':') != -1){
				ctable = thh.split(':')[1];
				thh = thh.replace(/:.+/, '');
			}
			var index = thh.split('_').slice(1).join('_');
			code.html(code.html().replace(mtc[k], 
				repStr
					.replace(/\{table\}/g, table)
					.replace(/\{index\}/g, index)
					.replace(/\{ctable\}/g, ctable)
			))
		}
		code.html(
			code.html().replace(/\/\*\*\*\*\*(.|\n)*\*\*\*\*\*\//gm, '')
		);
	}

</script>
<h1><?php echo $title; ?></h1>
<p>
	<h2>1. Код</h2>
	<h3>PHP</h3>
	<p>
		Далее приведу примеры кода из недавних проектов.
	</p>

	<p>
		<p class="desc">
			Класс, компонент Yii2, который генерирует карту тэгов для быстрой фильтрации
		</p>
		<pre class="invisible-code">
			<code class="php" >
<?php echo $this->render('php0'); ?>
			</code>
		</pre>
	</p>

	<p>
		<p class="desc">
			Сисетма AJAX-фильтрации для OpenCart. Для максимального быстродействия MVC.M упущен.
		</p>
		<pre class="invisible-code">
			<code class="php" >
<?php echo $this->render('php1'); ?>
			</code>
		</pre>
	</p>

	<p>
		<p class="desc">
			Генератор тестов для PsyTest. Laravel.
		</p>
		<pre class="invisible-code">
			<code class="php" >
<?php echo $this->render('php2'); ?>
			</code>
		</pre>
	</p>

	<h3>JS</h3>

	<p>
		<p class="desc">
			Компонент для древовидного отображения документации.
		</p>
		<pre class="invisible-code">
			<code class="javascript" >
<?php echo $this->render('js0'); ?>
			</code>
		</pre>
	</p>

	<p>
		<p class="desc">
			Кастомная библиотека для UI.
		</p>
		<pre class="invisible-code">
			<code class="javascript" >
<?php echo $this->render('js1'); ?>
			</code>
		</pre>
	</p>
	


	<h3>HTML</h3>
	<p>
		<p class="desc">
			HTML + Blade + Bootstrap
		</p>
		<pre class="invisible-code">
			<code class="html" >
<?php echo $this->render('html0'); ?>
			</code>
		</pre>
	</p>
	<h3>Python</h3>
	<p>
		<p class="desc">
			Парсер проектов для этого сайта
		</p>
		<pre class="invisible-code">
			<code class="python" >
<?php echo $this->render('py0'); ?>
			</code>
		</pre>
	</p>
	<p>
		<p class="desc">
			Конвертер дерева с изображениями via Imagick.
		</p>
		<pre class="invisible-code">
			<code class="python" >
<?php echo $this->render('py1'); ?>
			</code>
		</pre>
	</p>
	<br><br><br><br>
	<h2>2. Структура магазина</h2>
	<h3>Задание</h3>
		<div class="quote">
			Описать общую структуру БД (список таблиц и полей таблиц БД) для данного проекта какой вы её видите для создания сайта со следующими разделами: 
			<ul>
			<li>страница каталога товаров</li>
			<li>страница фильтра по каталогу (категории товаров)</li>
			<li>страница товара (карточка товара) (страница товара представляет собой несколько изображений товара, титул и описание товара, характеристики товара, стоимость товара, список сопутствующих товаров, артикул товара, наличие товара)</li>
			<li>корзина товаров</li>
			<li>статьи о товарах и о категориях товаров (для каждого товара и для каждой категории товаров может быть несколько статей)</li>
			</ul>
		</div>
	<h3>Ремарка</h3>
	<p>В тех. задании не указаны следующие детали: </p>
	<ul>
		<li>Мультиязычен ли проект?</li>
		<li>Насколько стандартизированы свойства?</li>
		<li>Нужно ли разбивать продукты по складам и суммировать их наличие в зависимости от того или иного склада?</li>
	</ul>
	<p>От мультиязычности зависит несколько факторов: </p>
	<ul>
		<li>Названия товаров для каждого языка - разные</li>
		<li>Цены для каждого языка - разные, указанные в валюте, которая понятна носителю языка</li>
	</ul>
	<br>
	<p>
		В соответствии с указанными неточностями приведу два варианта схем: с учетом мультиязычности и без. 
	</p>
	<p>
		Насчет стандартизации - будем считать, что у продукта есть обязательные свойства ( например: имя, артикул, количество) и необязательные, присущие каждому продукту/категории в отдельности. 
	</p>
	<p>
		Насчет складов - будем считать, что схема "в капсуле" - то есть не должна отражать информации о складах и расчете конечной стоимости с упором на них. Это нужно из соображения безопасности - чтобы даже в худшем случае, когда у злоумышленника есть полный доступ к БД, он ничего не знал о внутренней структуре предприятия. А если потребуется отображать информацию о количестве на каждом отдельном складе (для расчета времени доставки, например) - можно без проблем вывести таблицу product_storage  и из него расчитывать количество. 
	</p>
	<h3>Схема без учета мультиязычности:</h3>
	<pre><code class="mysql" >
	/*****
		Чтобы упростить чтение и убрать избыточность, я введу краткое обозначение:
		Выражение @table_index[:ctable] = 
			table_index INT,
			INDEX table_index (table_index),
			FOREIGN KEY (table_index) REFERENCES {ctable ?? table}(index) ON DELETE CASCADE
		/@
		То есть если есть запись @product_id, то на SQL это будет так:
			product_id INT,
			INDEX product_id (product_id),
			FOREIGN KEY (product_id) REFERENCES product(id) ON DELETE CASCADE
		А если так @parent_id:cat:
			parent_id INT,
			INDEX parent_id (parent_id),
			FOREIGN KEY (parent_id) REFERENCES cat(id) ON DELETE CASCADE
		Если хотите видеть Raw SQL, раскройте этот макрос, кликнув <span class="open" onclick="replaceMacro.call(this);">сюда</span>
		*****/
	#Категория, parent_id - ссылка на родительскую категорию
	CREATE TABLE cat(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		name CHAR(255) DEFAULT '',
		description TEXT,
		@parent_id:cat
	);
	#Продукт
	CREATE TABLE product(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		vendor_code varchar(1000),
		quantity INT DEFAULT 0,
		first_image CHAR(255),
		name CHAR(255) DEFAULT '',
		description TEXT,
		price float
	);
	#Отношение продукта к категории
	CREATE TABLE product_to_cat(
		@product_id,
		@cat_id
	);
	#Картинки продукта
	CREATE TABLE product_image(
		@product_id,
		filename CHAR(255)
	);

	#Атрибут - вынесен в отдельную таблицу для отношений как к продуктам, так и к категориям
	#Данное отношение ускоряет фильтрацию, то есть если выбрана какая-то категория, именно
	# из этой таблицы будут выбраны атрибуты, которые относятся к продуктам данной категории
	#Задан тип атрибута для усорения фильтрации
	CREATE TABLE attr(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		attr_name CHAR(255) DEFAULT '',
		attr_type enum ('text', 'float')
	);
	#Ссылка на атрибут и его значение
	CREATE TABLE product_variable_attr(
		@product_id,
		@attr_id,
		attr_val_text TEXT,
		attr_val_float FLOAT NOT NULL
	);
	#Ссылка на атрибут
	CREATE TABLE cat_variable_attr(
		@cat_id,
		@attr_id
	);
	#Статья
	CREATE TABLE article(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		heading CHAR(255) NOT NULL,
		short_description VARCHAR(1000),
		content TEXT
	);
	#Отношение продукта к статье
	CREATE TABLE product_to_article(
		@product_id,
		@article_id
	);
	#Юзверь
	CREATE TABLE user(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
		/* ... */
	);
	#Корзина юзверя
	CREATE TABLE user_bucket(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		create_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		@user_id
	);
	#Позиция в корзине юзверя - продукт и его количество
	CREATE TABLE user_bucket_product(
		@user_id,
		@product_id,
		quantity INT
	);
	#Сопутсвуюшие товары
	CREATE TABLE product_to_product(
		@parent_id:product,
		@child_id:product
	);
	</code></pre>
	<h3>Схема с учетом мультиязычноcти:</h3>
	<pre><code class="mysql" >
	/*****
		Чтобы упростить чтение и убрать избыточность, я введу краткое обозначение:
		Выражение @table_index[:ctable] = 
			table_index INT,
			INDEX table_index (table_index),
			FOREIGN KEY (table_index) REFERENCES {ctable ?? table}(index) ON DELETE CASCADE
		/@
		То есть если есть запись @product_id, то на SQL это будет так:
			product_id INT,
			INDEX product_id (product_id),
			FOREIGN KEY (product_id) REFERENCES product(id) ON DELETE CASCADE
		А если так @parent_id:cat:
			parent_id INT,
			INDEX parent_id (parent_id),
			FOREIGN KEY (parent_id) REFERENCES cat(id) ON DELETE CASCADE
		Если хотите видеть Raw SQL, раскройте этот макрос, кликнув <span class="open" onclick="replaceMacro.call(this);">сюда</span>
		*****/
	#Категория, parent_id - ссылка на родительскую категорию
	CREATE TABLE cat(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		@parent_id:cat
	);
	#Описание категории
	CREATE TABLE cat_const_attr(
		@cat_id,
		lang char(255) NOT NULL,
		name CHAR(255) DEFAULT '',
		description TEXT
	);
	#Продукт
	CREATE TABLE product(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		vendor_code varchar(1000),
		quantity INT DEFAULT 0,
		first_image CHAR(255)
	);
	#Отношение продукта к категории
	CREATE TABLE product_to_cat(
		@product_id,
		@cat_id
	);
	#Картинки продукта
	CREATE TABLE product_image(
		@product_id,
		filename CHAR(255)
	);
	#Постоянные атрибуты продукта, которые зависят от текущего языка
	CREATE TABLE product_const_attr(
		@product_id,
		lang char(255) NOT NULL,
		name CHAR(255) DEFAULT '',
		description TEXT
	);
	#Атрибут - вынесен в отдельную таблицу для отношений как к продуктам, так и к категориям
	#Данное отношение ускоряет фильтрацию, то есть если выбрана какая-то категория, именно
	# из этой таблицы будут выбраны атрибуты, которые относятся к продуктам данной категории
	#Задан тип атрибута для усорения фильтрации
	CREATE TABLE attr(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		lang char(255) NOT NULL,
		attr_name CHAR(255) DEFAULT '',
		attr_type enum ('text', 'float')
	);
	#Ссылка на атрибут и его значение
	CREATE TABLE product_variable_attr(
		@product_id,
		@attr_id,
		attr_val_text TEXT,
		attr_val_float FLOAT NOT NULL
	);
	#Ссылка на атрибут
	CREATE TABLE cat_variable_attr(
		@cat_id,
		@attr_id
	);
	#Цена продукта в валюте currency
	CREATE TABLE product_price(
		@product_id,
		currency char(255) NOT NULL,
		price FLOAT DEFAULT 0
	);
	#Статья
	CREATE TABLE article(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		lang CHAR(255) NOT NULL,
		heading CHAR(255) NOT NULL,
		short_description VARCHAR(1000),
		content TEXT
	);
	#Отношение продукта к статье
	CREATE TABLE product_to_article(
		@product_id,
		@article_id
	);
	#Юзверь
	CREATE TABLE user(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY
		/* ... */
	);
	#Корзина юзверя
	CREATE TABLE user_bucket(
		id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
		create_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
		lang char(255),
		currency char(255),
		@user_id
	);
	#Позиция в корзине юзверя - продукт и его количество
	CREATE TABLE user_bucket_product(
		@user_id,
		@product_id,
		quantity INT
	);
	#Сопутсвуюшие товары
	CREATE TABLE product_to_product(
		@parent_id:product,
		@child_id:product
	);
	</code></pre>
	<h3>Схема кэшированной фильтрации:</h3>
	<pre><code class="python" >
	#Для ускорения фильтрации можно создавать каждый раз временную таблицу,
	# которая будет обновляться при добавлении и изменении новых непостоянных аттрибутов
	#Данную таблицу нецелесообразно использовать при обновлении более частом, чем выборке
	#Псевдокод такой таблицы следующий
	'CREATE TABLE product_variable_attr_cached[_lang]('
		'product_id int,'
		for pva in product_variable_attr.filter( /*Here lambda that filters by lang if present*/ ):
			'attr_' + pva.attr.id + ' ' + typeSql(pva.attr.attr_type) + (last(pva) ? '' : ',')
		
	')'
	</code></pre>
</p>
