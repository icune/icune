<?php
	use yii\helpers\Url;
	use app\components\TagsWidget;
	use app\assets\ViewerJsAsset;
	ViewerJsAsset::register($this);
	$titleTag = isset($ind) ? 'h1' : 'h3';
?>
<<?php echo $titleTag; ?>>
	<?php echo $p->name;?>
</<?php echo $titleTag; ?>>

<table class="parameters">
	<tr>
		<td class="param-col">
			<?php echo Yii::$app->locTitle->get('#dates_title'); ?>
		</td>
		<td class="param-col">
			<?php echo $p->dateFromText . ' - ' . $p->dateToText;?>		
			<?php 
				echo $this->render('datebar', [
					'utters' => $utterDates,
					'dates' => [$p->date_from, $p->date_to]
				])
			 ?>
		</td>
	</tr>
	<tr>
		<td class="param-col">
			
			<?php echo Yii::$app->locTitle->get('#description_title'); ?>:
					
		</td>
		<td class="param-col">
			<?php echo $p->description_->current->content;?>
		</td>
	</tr>
	<tr>
		<td class="param-col">
			
			<?php echo Yii::$app->locTitle->get('#contribution_title'); ?>:

		</td>
		<td class="param-col">
			<?php echo $p->contribution_->current->content;?>
		</td>
	</tr>
	<tr>
		<td class="param-col">
			<?php echo Yii::$app->locTitle->get('#tags_title'); ?>
		</td>
		<td class="param-col">
			<?php echo TagsWidget::widget(['tags' => $p->tags]); ?>
		</td>
	</tr>
	<?php if(!isset($ind)): ?>
	<tr>
		<td class="param-col">
			<?php echo Yii::$app->locTitle->get('#link_title'); ?>
		</td>
		<td class="param-col">
			<a href="<?php echo Url::to('/projects/ind/'.$p->id);?>" target="_blank">
				<span class="glyphicon glyphicon-link" style="font-size:20px">
					
				</span>
			</a>
		</td>
	</tr>
	<tr>
		<td class="param-col">
			<?php echo Yii::$app->locTitle->get('#picture_title'); ?>
		</td>
		<td class="param-col">
			<span class="glyphicon glyphicon-eye-open view-picture"></span>
		</td>
	</tr>
<?php endif; ?>
</table>