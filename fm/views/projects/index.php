<?php 
	
	use app\assets\MixitupAsset;
	use app\assets\ProjectsAsset;
	use app\components\TagsWidget;
	MixitupAsset::register($this);
	ProjectsAsset::register($this);
	function tagFilter($tag){
		return str_replace('+', 'p', $tag->name);
	}
	$this->title = Yii::$app->locTitle->get('#menu_projects_title');
?>
<h1>
	<?php echo Yii::$app->locTitle->get('#menu_projects_title'); ?>
</h1>
<div class="row filter-tags">
	<?php 
		$class = (int)(12/count($types));
	?>
	<?php foreach ($types as $type=>$tags): ?>
		<div class="col-md-<?php echo $class;?>">
			<div>
				<h4><?php echo Yii::$app->locTitle->get('#'.$type); ?></h4>
			</div>
			<?php echo TagsWidget::widget(['tags' => $tags]) ?>
		</div>
	<?php endforeach ?>
</div>
<div class="row" id="projects">
<?php foreach($projects as $k => $p): ?>
		
			
		
		<div 
			class="col-md-6 col-xs-12 project mix <?php echo implode(' ', array_map('tagFilter', $p->tags));?>"
			image="/img/project/<?php echo $p->image;?>"	
		>
			<div class="abs text-container">
				<?php echo $this->render('project-info', ['p' => $p, 'utterDates' => $utterDates]); ?>
			</div>
		</div>
	
<?php endforeach; ?>
</div>

<script type="text/javascript">
	window.tagMap = <?php echo json_encode($tagMap); ?>;
	window.filters = <?php echo json_encode($filters); ?>;
	window.canonicalUrl = '<?php echo $canonicalUrl; ?>';
</script>

