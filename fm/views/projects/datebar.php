<?php 
	if(!function_exists('dateIval')){
		function dateIval($from, $to){
			$begin = new DateTime( $from);
			$end = new DateTime( $to );
			$end = $end->modify( '+1 day' ); 

			$interval = new DateInterval('P1M');
			$daterange = new DatePeriod($begin, $interval ,$end);
			$months = [];
			foreach ($daterange as $dr) {
				$months []= $dr->format('m.Y');
			}
			return $months;
		};
		function thinOut($a){
			$r = [];
			foreach($a as $k => $_a)
				if($k % 6 == 0)
					$r []= $_a;
				else
					$r []= '';
			return $r;
		}
		function minText($txt){
			$p = explode('.', $txt);
			return (int)$p[0] . '.' .substr($txt, -2, 2);
		}

	};
	$months = dateIval($utters[0], $utters[1]);
	$thinMonths = thinOut($months);
	$datesIn = dateIval($dates[0], $dates[1]);
	$width = 'width:' . 100/count($months) . '%';
	// echo '<br>';
	// echo $dates[0];
	// echo '<br>';
	// echo $dates[1];
	// echo '<br>';
	// print_r($datesIn);
	// echo '<br>';
	// print_r($months);
?>
<div class="datebar">
	<div class="labels">
		<table>
			<tr>
				<?php foreach ($months as $k => $m): ?>
					<td style="<?php echo $width; ?>">
						<div class="kuku-text">
							<?php if($thinMonths[$k]): ?>	
								<span class="date-point">&nbsp;</span>
								<?php echo minText($thinMonths[$k]); ?>
							<?php endif; ?>	
							
						</div>
					</td>
				<?php endforeach ?>
			</tr>
		</table>
	</div>
	<div class="pic">
		<table>
			<?php foreach ($months as $m): ?>
				<td title="<?php echo $m; ?>" style="<?php echo $width; ?>" class="<?php echo in_array($m, $datesIn) ? 'active' : ''; ?>">
					
				</td>
			<?php endforeach ?>
		</table>
	</div>
</div>
