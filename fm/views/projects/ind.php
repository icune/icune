<?php 
	
	use app\assets\MixitupAsset;
	use app\assets\ProjectsAsset;
	MixitupAsset::register($this);
	ProjectsAsset::register($this);
	function tagFilter($tag){
		return str_replace('+', 'p', $tag);
	}
	$this->title = $project->name;
?>
<style>
	.project{
		display: block;
		border: none;
	}
	.project:after{
		display: none;
	}
	#ifr{
		width:100%;
		height:700px;
	}
</style>
<div class="project">
	<?php echo $this->render('project-info', [
		'p' => $project, 'utterDates' => $utterDates, 'ind' => true
	]); ?>
</div>
<h3>
	<a href="http://<?php echo $project->linkText?>" target="_blank">
		<span class="glyphicon glyphicon-link" style="font-size:20px">
			
		</span>	
		<?php echo Yii::$app->locTitle->get('#go_to_href_title'); ?>
	</a>
	

</h3>
<iframe id="ifr" src="http://<?php echo $project->linkText; ?>" frameborder="0">
	
</iframe>
<script type="text/javascript">
	window.tagMap = [];
	window.filters = [];
</script>