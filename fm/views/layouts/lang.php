<?php
use yii\helpers\url;
?>
<<?=isset($mobile) ? 'div':'li';?> class="<?=isset($mobile) ? 'lang-external-button visible-xs':'hidden-xs';?>">
    
            
            <table class="centerer">
            	<tr class="centerer-tr">
            		<td></td>
            	</tr>
            	<tr class="centerer-tr">
            		<td>
	            		<form action="<?=Url::current();?>" method="GET" class="lang">
					        <input type="hidden" name="lang" value="<?=Yii::$app->loc->to()?>">
					        <button class="lang-button">
	            				<img src="/img/lang/<?=Yii::$app->loc->to()?>.png" alt="">
	            			</button>
	    				</form>
            		</td>
            	</tr>
            	<tr class="centerer-tr">
            		<td></td>
            	</tr>
            </table>
			
        
</<?=isset($mobile) ? 'div':'li';?>>