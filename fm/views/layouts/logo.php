<a class="navbar-brand" 
    href="/"
    <?=Yii::$app->request->pathInfo?'':'onclick="return false;"';?>
>
    <div class="logo-container">

        
        <table class="centerer">
            <tr class="centerer-tr">
                <td></td>
            </tr>
            <tr class="centerer-tr">
                <td>
                    <table class="logo">
                        <tr>
                            <td>
                                <div part-name="lt" class="part white"></div>
                            </td>
                            <td>
                                <div part-name="rt" class="part round green"></div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div part-name="lb" class="part blue"></div>
                            </td>
                            <td>
                                <div part-name="rb" class="part"></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="centerer-tr">
                <td></td>
            </tr>
        </table>
    </div>
</a>