<?php
	use app\components\TagsWidget;
	use yii\helpers\Url;
	$this->title = Yii::$app->locTitle->get('#main');
?>

<p>
	
	<h1><?php echo Yii::$app->locTitle->get('#main'); ?></h1>
	
	<div class="row about">
		<div class="col-md-3">
			<img src="/img/face.jpg" width="200" style=" margin:10px;">
		</div>
		<div class="col-md-9">
			<?php 
				$desc = Yii::$app->locTitle->get('#main_page'); 
				$desc = str_replace('{tech}', TagsWidget::widget([
					'miniList' => true,
					'ob' => function($tag, $html){
						$href = Url::toRoute('projects/index/' . $tag->name);
						echo "<a href='$href'>$html</a>";
					}
				]), $desc);
				$href = Url::toRoute('projects/index');
				$a = "<a href='$href'><span class='glyphicon glyphicon-link' style='font-size:20px'></span></a>";
				$desc = str_replace('{link}', $a, $desc);
				echo $desc;
			?>
		</div>
	</div>
	


</p>