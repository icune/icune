<?php
	$this->title = Yii::$app->locTitle->get('#contacts');
?>
<h1><?php echo Yii::$app->locTitle->get('#contacts'); ?></h1>
<style>	

</style>
<p>
    <h4 class="eu">
    	GitHub: <a href="https://github.com/icune" target="_blank">
    			https://github.com/icune
    		</a>
	</h4>
    
	<h4 class="eu">
		BitBucket: <a href="https://bitbucket.org/icune" target="_blank">
    			https://bitbucket.org/icune
    		</a>
	</h4>
    
    <h4 class="eu">
    	VK: <a href="https://vk.com/icune" target="_blank">
    			https://vk.com/icune
    		</a>
	</h4>
   	
	<h4 class="eu">
		FB: <a href="https://www.facebook.com/ivan.levitsky.5" target="_blank">
    			https://www.facebook.com/ivan.levitsky.5
    		</a>
	</h4>
   	<h4 class="eu">
		E-mail: <a href="mailto:icune@ya.ru" target="_blank">
    			icune@ya.ru
    		</a>
	</h4>
	<h4 class="eu">
		Telegram: <a href="https://telegram.me/icune" target="_blank">
    			@icune
    		</a>
	</h4>
</p>
