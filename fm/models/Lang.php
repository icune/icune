<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lang".
 *
 * @property integer $id
 * @property string $name
 *
 * @property LangContent[] $langContents
 * @property Project[] $projects
 * @property Project[] $projects0
 */
class Lang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContent()
    {
        return $this->hasOne(LangContent::className(), ['lang_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrent()
    {
        return $this->hasOne(LangContent::className(), ['lang_id' => 'id'])->andWhere([
            'lang' => Yii::$app->loc->get()
        ]);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContents()
    {
        return $this->hasMany(LangContent::className(), ['lang_id' => 'id']);
    }
}
