<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "project_tag".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $tag_id
 *
 * @property Project $project
 * @property Tag $tag
 */
class ProjectTag extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_tag';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'tag_id'], 'required'],
            [['project_id', 'tag_id'], 'integer'],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tag::className(), 'targetAttribute' => ['tag_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'project_id' => Yii::t('app', 'Project ID'),
            'tag_id' => Yii::t('app', 'Tag ID'),
        ];
    }

    public function getTag()
    {
        return $this->hasOne(Tag::className(), ['id' => 'tag_id']);
    }
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['id' => 'project_id']);
    }
    public function __toString()
    {
        return $this->tag->name;
    }
}
