<?php

namespace app\models;
use app\components\ImageColor;
use Yii;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $date_from
 * @property string $date_to
 * @property integer $description
 * @property integer $contribution
 * @property string $name
 * @property string $link
 * @property string $code_link
 * @property string $image
 *
 * @property Lang $contribution0
 * @property Lang $description0
 * @property ProjectTag[] $projectTags
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_from', 'date_to'], 'safe'],
            [['description', 'contribution'], 'required'],
            [['description', 'contribution'], 'integer'],
            [['name', 'link', 'code_link', 'image'], 'string', 'max' => 255],
            [['contribution'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['contribution' => 'id']],
            [['description'], 'exist', 'skipOnError' => true, 'targetClass' => Lang::className(), 'targetAttribute' => ['description' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date_from' => Yii::t('app', 'Date From'),
            'date_to' => Yii::t('app', 'Date To'),
            'description' => Yii::t('app', 'Description'),
            'contribution' => Yii::t('app', 'Contribution'),
            'name' => Yii::t('app', 'Name'),
            'link' => Yii::t('app', 'Link'),
            'code_link' => Yii::t('app', 'Code Link'),
            'image' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContribution_()
    {
        return $this->hasOne(Lang::className(), ['id' => 'contribution']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDescription_()
    {
        return $this->hasOne(Lang::className(), ['id' => 'description']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable(
            'project_tag', ['project_id' => 'id']
        );
    }

    public function getDateFromText(){
        return date("m.Y", strtotime($this->date_from));
    }
    public function getDateToText(){
        return date("m.Y", strtotime($this->date_to));
    }
    /**
     * @return string = link with current domain inside
     */
    public function getLinkText(){
        return str_replace('domain.com', isset($_SERVER['HTTP_HOST'])?$_SERVER['HTTP_HOST'] : 'ururu.ru', $this->link);
    }
    /**
     * @return array(2) = [min_date, max_date]
     */
    public static function utterDates(){
    	$C = Yii::$app->cache;
    	$got = $C->get('utter_dates');
    	if($got)
    		return $got;
    	$minDate = self::find() 
		    ->select('min(date_from)') 
		    ->scalar();
	    $maxDate = self::find() 
		    ->select('max(date_to)') 
		    ->scalar();

		if(!$got)
			$C->set('utter_dates', [$minDate, $maxDate]);
		return [$minDate, $maxDate];
    }
    /**
     * @return color, that calculated from Projecr's image
     */
    public function getImageColor(){
        $path = \Yii::getAlias('@webroot') . '/img/project/';
        $iColor = new ImageColor;
        return $iColor->get($path . $this->image);
    }
}
