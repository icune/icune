<?php
	namespace app\components;
	class Loc extends \yii\base\Component{
		private $lang;
	    public function init() {
	        parent::init();

	        $session = \Yii::$app->session;
	        
	        $postLang = isset($_GET['lang']) ? $_GET['lang'] : null;

	        if(in_array($postLang, ['en', 'ru'])){
	        	$session->set('lang', $postLang);
	        	$this->lang = $postLang;
	        }else if($session->has('lang')){
	        	$this->lang = $session->get('lang');
	        }else{
	        	if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
	        		$lang = strtolower(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2));
	        	else
	        		$lang = 'ru';
	        	if(!in_array($lang, ['ru', 'en']))
	        		$lang = 'ru';
	        	$session->set('lang', $lang);
	        	$this->lang = $lang;
	        }
	        
	    }
	    public function get(){
	    	return $this->lang;
	    }
	    public function to(){
	    	return $this->get() == 'ru' ? 'en' : 'ru';
	    }
	}