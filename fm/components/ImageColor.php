<?php
	namespace app\components;
	use app\models\Project;
	class ImageColor extends \yii\base\Component{
	    public function get($fileName){
	    	$cacheKey = '#colorOf_' . $fileName;
	    	$got = \Yii::$app->cache->get($cacheKey);
	    	if($got) return $got;

	    	list($r, $g, $b) = [0, 0, 0];
	    	$im = imagecreatefrompng($fileName);
	    	$w = imagesx($im);
	    	$h = imagesy($im);
	    	for($i = 0; $i < $w; $i++)
	    		for($j = 0; $j < $h; $j++){
	    			$rgb = imagecolorat($im, $i, $j);
					$r += ($rgb >> 16) & 0xFF;
					$g += ($rgb >> 8) & 0xFF;
					$b += $rgb & 0xFF;
	    		}
	    	$r /= $w*$h;
	    	$g /= $w*$h;
	    	$b /= $w*$h;

	    	$hex = 
	    		str_pad(dechex($r), 2, '0', STR_PAD_LEFT) .
	    	 	str_pad(dechex($g), 2, '0', STR_PAD_LEFT) .
	    	 	str_pad(dechex($b), 2, '0', STR_PAD_LEFT) ;

	    	\Yii::$app->cache->set($cacheKey, $hex);
	    	return $hex;
	    }
	}