<?php
	namespace app\components;
	use app\models\Lang;
	class LocTitle extends \yii\base\Component{
		private $lang;
	    public function init() {
	        parent::init();
	        $this->lang = \Yii::$app->loc->get();
	        
	    }

	    public function get($title){
	    	$C = \Yii::$app->cache;
	    	$got = $C->get($title . '___' .$this->lang);
	    	if($got){
	    		return $got;
	    	}
	    	$lang = Lang::findOne(['name' => $title]);
			if(!$lang || !$lang->contents)
				return $title;
			
			$contents = $lang->contents;
	    	if(!$contents || empty($contents))
	    		return $title;
	    	foreach($contents as $c)
	    		if($c->lang == $this->lang){
	    			$C->set($title . '___' .$this->lang, $c->content);
	    			return $c->content;
	    		}
	    	return $title;
	    }
	}