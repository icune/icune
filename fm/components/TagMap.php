<?php
	namespace app\components;
	use app\models\Project;
	class TagMap extends \yii\base\Component{
		/*
			Returns array k=>v, where k - project id, k:array - tags of project
			@return array
		*/
	    public function get(){
	    	$C = \Yii::$app->cache;
	    	$got = $C->get('tag_map');
	    	if($got)
	    		return $got;
	    	$tagMap = [];
	    	$projects = Project::find()->all();
	        
            foreach($projects as $project){
            		$tagMap[$project->id] = [];
                    foreach($project->tags as $_tag)
                        $tagMap[$project->id] []= $_tag->name;
            };
	        
	        if(!$got)
	        	$C->set('tag_map', $tagMap);
	        return $tagMap;
	    }
	}