<?php
	namespace app\components;

	use yii\base\Widget;
	use yii\helpers\Html;
	use app\models\Tag;

	class TagsWidget extends \yii\base\Widget{
		public $tags, $miniList = false, $ob;
		public function init(){
			parent::init();
			if($this->tags === null)
				$this->tags = Tag::find()->all();
			if($this->ob === null)
				$this->ob = function($tag, $html){echo $html;};
		}
		public function run()
		{
			
		    return $this->render('widgets/tags', [
		    	'tags' => $this->tags,
		    	'miniList' => $this->miniList,
		    	'ob' => $this->ob
		    ]);
		}
	}