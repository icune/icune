<?php 
use app\assets\TagsAsset;
TagsAsset::register($this);
?>
<div class="tags">
	<?php foreach ($tags as $tag): ?>
		<?php ob_start(); ?>
			<?php if ($miniList): ?>
				<div class="mini tag">
					<table>
						<tr>
							<td>
								<img  src="
									/img/tags/<?php echo $tag->name; ?>.png
								" alt="">
							</td>
							<td>
								<?php echo $tag->name; ?>			
							</td>
						</tr>
					</table>
					
					
				</div>
			<?php else: ?>
				<div class="tag"  data-toggle="popover" data-trigger="hover" data-placement="top" data-content="<?php echo $tag->name ?>" tag="<?php echo $tag->name; ?>">
					<img  src="
						/img/tags/<?php echo $tag->name; ?>.png
					" alt="">
				</div>
			<?php endif ?>
		<?php $content = ob_get_contents(); ?>
		<?php ob_end_clean(); ?>
		<?php $ob($tag, $content); ?>
	<?php endforeach ?>
</div>