<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HighlightAsset extends AssetBundle
{
    public $css = [
        'com/highlightjs/cd.css'
    ];
    public $js = [
        'com/highlightjs/cd.js',
        'com/highlightjs/init.js'
    ];
    public $depends = [
        'app\assets\AppAsset',
    ];
}
