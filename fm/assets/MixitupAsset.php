<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MixitupAsset extends AssetBundle
{
    public $sourcePath = '@bower/mixitup2';
    public $css = [
        
    ];
    public $js = [
        'src/jquery.mixitup.js'
    ];
    public $depends = [
        'app\assets\AppAsset',
    ];
}
