print 'Cannot change tables (lang, etc.)'
exit();

import re 

def parse(lines, regexp):
	tl = []
	preFound = None
	for l in lines:
		found = re.findall(regexp, l)
		if found:
			if len(tl):
				yield (tl, preFound[0])
				tl = []
			tl = [l]
			# print tl
			preFound = found
		elif len(tl):
			# print tl
			tl.append(l)
	if len(tl):
		# print tl
		yield (tl, preFound[0])

def toDict2Br(cort):
	"""
	cort[0] - lines
	cort[1] - result of findall, where must be such pattern : ...((...)...)...
	"""
	
	return (cort[1][1], [cort[0][0].replace(cort[1][0], '')] + (cort[0][1:] if len(cort[0]) > 1 else []))
def remLines(lines_from, lines_what):
	for l in lines_what:
		if l in lines_from:
			lines_from.remove(l)
	return lines_from

a = []
with open('desc.txt') as f:
	a = f.read()

r = {}
for a in parse(a.split("\n"), "^((.+?\[.+\]))"):
	k, lines = toDict2Br(a)
	# print '['+k+']', lines
	r[k] = {"lines" : lines, "params":{}}
	for b in parse(lines[:], '^(([a-zA-Z]+):).*'):
		r[k]['lines'] = remLines(r[k]['lines'], b[0])
		kk, lines2 = toDict2Br(b)
		r[k]["params"][kk] = lines2


import json
with open('desc.json', 'w') as f:
	f.write(json.dumps(r))


import mysql.connector
import re
class MS:
	cnx = None
	cursor = None
	emulation = False
	def __init__(self, emulation = False):
		self.cnx = mysql.connector.connect(user='root', database='ask_guru')
		self.cursor = self.cnx.cursor()
		self.emulation = emulation
	def insert(self, table, values):
		sql = ("INSERT INTO " + table + " "
               "(" + ",".join(values.keys()) + ") "
               "VALUES (" + ",".join(["'"+str(v)+"'" for v in values.values()]) +")")
		print sql
		data = ()
		if not self.emulation:
			self.cursor.execute(sql, data)
			self.cnx.commit()
		return self.cursor.lastrowid
	def delete(self, table, where = None):
		sql = ("DELETE FROM "   + table +  " " +
               (
               	"WHERE " + " AND ".join([k + " IN (" + 
               	','.join(['"'+str(_v) + '"' for _v in v])
               	+")" for k, v in where.iteritems()]) if where is not None else ''
               )
               )
		print sql
		data = ()
		if not self.emulation:
			self.cursor.execute(sql, data)
			self.cnx.commit()
	def resetAutoinc(self, table):
		sql = 'ALTER TABLE %s AUTO_INCREMENT=1'%table
		data = ()
		print sql
		if not self.emulation:
			self.cursor.execute(sql, data)
			self.cnx.commit()

tagInfo = {
	'@' : 'Skill',
	'@!' : 'Technology',
	'@@' : 'Framework'
}

m = MS()
m.delete('tag')
m.delete('project')
m.delete('project_tag')
m.delete('lang')
m.delete('lang_content')

m.resetAutoinc('tag')
m.resetAutoinc('project')
m.resetAutoinc('project_tag')
m.resetAutoinc('lang')
m.resetAutoinc('lang_content')

def trDate(date, first = False):
	def resp(s):
		return re.sub('\s+', '', s)
	return resp(date.split('.')[1]) + '-' + resp(date.split('.')[0])	 + '-' +(
		'1' if first else '28'
	)
def getTags(tags):
	parts = filter(lambda e:e, re.split('\s+', tags))
	result = []
	for p in parts:
		found = re.search('^@(?:@|!)?', p)
		if not found:
			raise Exception('Wrong tag format')
		tag_type = tagInfo[found.group(0)]
		tag_name = re.sub('^@(?:@|!)?', '', p)
		result += [{
			"name" : tag_name,
			"type" : tag_type
		}]
	return result
tagIds = {}
def tagsIds(tags):
	tags = getTags(tags)
	ids = []
	for tag in tags:
		if not tag['name'] in tagIds:
			tagIds[tag['name']] = m.insert('tag', tag)	
		ids += [tagIds[tag['name']]]
	return ids


for k, pro in r.iteritems():
	tagsIds(' '.join(pro['params']['Tags']))

def lang(lines, name):
	en = []
	ru = []
	current = en
	for l in lines:
		l = re.sub('^\s+', '', l)
		if re.match('^RU>>> ', l):
			l = re.sub('^RU>>> ', '', l)
			current = ru
		current += [l]
	lang_id = m.insert('lang', {"name":name})
	lang_en = m.insert('lang_content', {
		"lang" : "en",
		"lang_id" : lang_id,
		"content" : "\n".join(en)
	})
	lang_ru = m.insert('lang_content', {
		"lang" : "ru",
		"lang_id" : lang_id,
		"content" : "\n".join(ru)
	})
	return lang_id

for k,v in r.iteritems():
	nameHref =  re.findall('(.*)\[(.*)]', k)[0]
	project_name = nameHref[0]
	project_link = nameHref[1]
	dates = re.split('\s*-\s*', v['params']['Dates'][0])
	project_date_from = trDate(dates[0], True)
	project_date_to = trDate(dates[1])
	info = {
		'name' : project_name,
		'link' : project_link,
		'date_from' : project_date_from,
		'date_to' : project_date_to,
	}
	contribution_id = lang(v['params']['Contribution'], 'project_contribution')
	description_id = lang(v['params']['Description'], 'project_description')
	info['contribution'] = contribution_id
	info['description'] = description_id

	project_id = m.insert('project', info)
	tags_ids = tagsIds(' '.join(v['params']['Tags']))
	
	for _id in tags_ids:
		m.insert('project_tag', {
			"project_id" : project_id,
			"tag_id" : _id
		})




