function render(url){
	var page = require('webpage').create();
	page.viewportSize = {
	  width: 1200,
	  height: 300
	};
	page.onError = function(msg, trace) {
	    var msgStack = ['ERROR: ' + msg];
	    if (trace && trace.length) {
	        msgStack.push('TRACE:');
	        trace.forEach(function(t) {
	            msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
	        });
	    }
	    // uncomment to log into the console 
	    // console.error(msgStack.join('\n'));
	};
	page.onResourceError = function(resourceError) {
	    console.error(resourceError.url + ': ' + resourceError.errorString);
	};
	page.open(url, function(status) {
	  console.log(status);
	  page.render('img/'+url.replace('http://', '') + '.png');
	  // phantom.exit();
	});
}
var urls = [
	'moscvetok.ru',
	'legalspace.world',
	'localhost',
	'kmeans.localhost',
	'bbq.localhost',
	'zemres50.ru',
	'boiler.localhost',
	'finser.localhost',
	'sanete.ru',
	'psytest.localhost',
	'ege-centr.ru',
	'nesa.info',
	'autoshtab.com',
	'a-perspektiva.ru',
	'ils.localhost',
	'carex.localhost',
];
// for(var k in urls)
// 	render('http://' + urls[k]);
render('http://bbq.localhost');