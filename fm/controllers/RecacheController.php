<?php

namespace app\controllers;

class RecacheController extends \yii\web\Controller
{
    public function actionIndex()
    {
    	\Yii::$app->cache->flush();
        return $this->redirect('/');
    }

}
