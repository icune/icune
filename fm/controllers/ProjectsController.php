<?php

namespace app\controllers;
use \app\models\Project;
use \app\models\Tag;
use yii\helpers;
class ProjectsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $filters = array_filter(explode(':', \Yii::$app->request->get('params')));
        $tags = Tag::find()->all();
        $types = [];
        foreach ($tags as $t) {
            $types[$t->type] = isset($types[$t->type]) ? array_merge($types[$t->type], [$t]) : [$t];
        }
        return $this->render('index', [
            'projects' => Project::find()->orderBy('date_from')->all(),
            'tags' => $tags,
            'types' => $types,
            'tagMap' => \Yii::$app->tagMap->get(),
            'utterDates' => Project::utterDates(),
            'filters' => $filters,
            'canonicalUrl' => \yii\helpers\Url::canonical()
        ]);
    }

    public function actionView()
    {
        return $this->render('view');
    }
    public function actionInd(){
        $project_id = (int)(\Yii::$app->request->get('params'));
        $project = Project::findOne($project_id);
        // if(!$project_id)
            // return redirect('/');
        return $this->render('ind', [
            'utterDates' => Project::utterDates(),
            'project' => $project
        ]);
    }

    public function behaviors()
    {
        return [
            'pageCache' => [
                'class' => 'yii\filters\PageCache',
                'only' => ['index'],
                'duration' => 60*60,
                
                'variations' => [
                    \Yii::$app->loc->get(),
                    \Yii::$app->request->get('params')
                ]
            ],
        ];
    }

}
