$(function(){
	var ls = window.localStorage;
	function setLs(part, _class){
		if(!ls)
			return;
		if(!ls['partsClicked'])
			ls['partsClicked'] = JSON.stringify({});
		var pc = JSON.parse(ls['partsClicked']);
		pc[part] = _class;
		ls['partsClicked'] = JSON.stringify(pc);
	}
	function getLs(part){
		if(!ls)
			return '';
		if(!ls['partsClicked'])
			return '';
		return JSON.parse(ls['partsClicked'])[part];
	}
	$('table.logo .part').click(function(){
		var _class = 'clicked';
		if($(this).hasClass(_class)){
			setLs($(this).attr('part-name'), '');
			$(this).removeClass(_class);
		}else{
			setLs($(this).attr('part-name'), _class);
			$(this).addClass(_class);
		}
	})
	$('table.logo .part').each(function(){
		$(this).addClass(getLs($(this).attr('part-name')));
	})
})


$("[data-toggle=popover]").popover();

