function TagManager(tagMap){
	var selectedTags = [];
	var relatedTags = [];
	this.add = function(el){
		if(selectedTags.indexOf(el) == -1)
			selectedTags.push(el);
		outval(el);
		recount();
	}
	this.remove = function(el){
		if(selectedTags.indexOf(el) != -1)
			selectedTags.splice(selectedTags.indexOf(el), 1);
		recount();
	}
	this.toggle = function(el){
		if(selectedTags.indexOf(el) != -1)
			this.remove(el);
		else
			this.add(el);
	}
	this.related = function(){
		return relatedTags;
	}
	this.selected = function(){
		return selectedTags;
	}
	this.reset = function(newTags){
		selectedTags = [];
		relatedTags = [];
		for(var k in newTags)
			this.add(newTags[k]);
	}
	function isect(a1, a2){
		if(!a2 || !a2.length) return a1;
		var ret = [];
		for(var k in a1)
			if(a2.indexOf(a1[k]) != -1)
				ret.push(a1[k]);
		return ret;
	}
	function aInA(needle, hay){
		for(var k in needle)
			if(hay.indexOf(needle[k]) == -1)
				return false;
		return true;
	}
	function recount(){
		var related = [];
		for(var k in tagMap){
			if(aInA(selectedTags, tagMap[k]))
				related = related.concat(tagMap[k]);
		}
		relatedTags = related;
	}
	function outval(el){
		if(relatedTags.indexOf(el) == -1){
			selectedTags = [el];
		}
	}
}
function History(){
	this.push = function(tags){
		if(!window.history)
			return;
		window.history.pushState(tags, tags.join(', '), canonicalUrl + '/' + tags.join(':'));
	}
	this.onRestate = function(cb){
		if(!window.history)
			return;
		window.addEventListener('popstate', function(e) {
			console.log(e);
			cb(e);   
		});
	}
}
function tagFilter(tag){
	return tag.replace(/\+/g, 'p');
}
$(function(){
	try{
		var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
	}catch(e){
		var isMobile = false;
	}
	if(true || !isMobile)
		$('#projects').mixItUp({
			
		});

	var atSel = '.filter-tags .tag';
	var tm = new TagManager(tagMap);
	var hi = new History();

	tm.reset(filters);
	function gFilter(){
		$(atSel).addClass('disabled');
		$(atSel).removeClass('selected');
		$(
			tm.selected().map(function(e){return atSel+'[tag="'+e+'"]';}).join(',')
		).addClass('selected');
		
		var related = tm.related();
		if(!related.length)
			$(atSel).removeClass('disabled')
		else
			$(
				related.map(function(e){return atSel+'[tag="'+e+'"]';}).join(',')
			).removeClass('disabled');
		filterIt();
	}

	$(atSel).click(function(){
		var tag = $(this).attr('tag');
		tm.toggle(tag);
		gFilter();
		hi.push(tm.selected());
	});
	gFilter();

	hi.onRestate(function(e){
		if(!e || !e.state)
			return;
		tm.reset(e.state);
		gFilter();
	});
	
	function filterIt(){
		
		var tags = [];
		$(atSel+'.selected').each(function(){
			tags.push('.'+tagFilter($(this).attr('tag')));
		});
		if(!tags.length)
			tags = ['.mix'];
		if(false &&  isMobile){
			$('#projects .project').hide();
			$('#projects .project'+tags.join('')).show();
		}else{
			$('#projects').mixItUp(
				'filter',tags.join('')
			);
		};
	}
})

$(function(){
	$('.project .view-picture').click(function(){
		var im = $('<img src="' + $(this).closest('.project').attr('image') + '">').appendTo('body').hide();
		var viewer = (new Viewer(im.get(0), {
			navbar:false, 
			hidden:function(){
				im.remove();
			},
			
			title:false,
			zoomRatio:0.7,

		})).show()
		viewer.zoom(0.8);
	})
	
})