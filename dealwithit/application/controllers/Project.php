<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('auth');
		if(!auth_ok()){
			redirect(site_url());
		}
	}
	public function index()
	{
		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		try{
			$crud = new grocery_CRUD();

			// $crud->set_theme('datatables');
			$crud->set_table('project');
			$crud->set_subject('Project');
			$crud->limit(100);
			// $crud->set_relation('contribution','lang', 'name');
			$crud->callback_column('contribution', [$this, '_lang_column']);
			$crud->callback_column('description', [$this, '_lang_column']);
			$crud->set_field_upload('image','../img/project');
			// $crud->required_fields('city');
			// $crud->columns('city','country','phone','addressLine1','postalCode');

			$output = $crud->render();

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
		$this->load->view('project', $output);
	}
	public function ptag()
	{
		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		try{
			$crud = new grocery_CRUD();

			// $crud->set_theme('datatables');
			$crud->set_table('project_tag');
			$crud->set_subject('Project tags');
			$crud->limit(100);
			// $crud->set_relation('contribution','lang', 'name');
			$crud->set_relation('project_id','project', 'name');
			$crud->set_relation('tag_id','tag', 'name');
			// $crud->required_fields('city');
			// $crud->columns('city','country','phone','addressLine1','postalCode');

			$output = $crud->render();

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
		$this->load->view('project', $output);
	}
	public function tags()
	{
		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		try{
			$crud = new grocery_CRUD();

			// $crud->set_theme('datatables');
			$crud->set_table('tag');
			$crud->set_subject('Tag');
			$crud->limit(100);

			$output = $crud->render();

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
		$this->load->view('project', $output);
	}
	public function _lang_column($value, $row){
		$ru_id = $this->db->query(
			"SELECT * FROM  lang_content WHERE lang='ru' AND lang_id=$value"
		)->result_array()[0]['id'];
		$en_id = $this->db->query(
			"SELECT * FROM  lang_content WHERE lang='en' AND lang_id=$value"
		)->result_array()[0]['id'];
		$ru_link = site_url('project/lang/edit/' . $ru_id);
		$en_link = site_url('project/lang/edit/' . $en_id);
		return "<a href='$ru_link' target='_blank'>ru</a> <a href='$en_link' target='_blank'>en</a>";
	}
	public function lang(){
		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		try{
			$crud = new grocery_CRUD();

			// $crud->set_theme('datatables');
			$crud->set_table('lang_content');
			$crud->set_relation('lang_id','lang', 'name');
			$crud->unset_texteditor('content');
			$output = $crud->render();

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
		$this->load->view('project', $output);
	}
	public function langrec(){
		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		try{
			$crud = new grocery_CRUD();

			// $crud->set_theme('datatables');
			$crud->set_table('lang');
			
			$output = $crud->render();

		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
		$this->load->view('project', $output);
	}
	public function qlang(){
		$this->load->database();
		$this->load->view('qlang');	
		if(isset($_POST['qlang'])){
			$qLang = $_POST['qlang'];
			if(preg_match('!^(.+?)::(.+?)::(.+?)$!', $qLang, $mtc)){
				array_shift($mtc);
				$this->db->query('INSERT INTO lang(name) VALUES(?)', [$mtc[0]]);
				$id = $this->db->insert_id();
				$this->db->query('INSERT INTO lang_content(lang_id, lang, content) VALUES(?,?,?)', 
					[$id, 'en', $mtc[1]]
				);
				$this->db->query('INSERT INTO lang_content(lang_id, lang, content) VALUES(?,?,?)', 
					[$id, 'ru', $mtc[2]]
				);
			}
		}
	}
}
