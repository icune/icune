<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		session_start();
		if(isset($_POST['secret'])){
			$_SESSION['secret'] =  $_POST['secret'];
			$this->load->helper('auth');
			$this->load->helper('url');
			if(auth_ok())
				redirect(site_url('/project'));
		};
		$this->load->helper('auth');
		
		$this->load->view('welcome_message');
	}
	public function logout()
	{
		$this->load->helper('url');
		$this->load->helper('auth');
		if(auth_ok()){
			unset($_SESSION['secret']);
			redirect(site_url('/project'));
		};
		
		redirect(site_url('/'));
	}
}
